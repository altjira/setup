#!/usr/bin/env bash
cd ~/.var/app/org.gnome.Builder/cache/gnome-builder/projects/carbon-setup/builds/default-host-main/
rm po/carbon-setup.pot
ninja carbon-setup-pot
ninja carbon-setup-update-po
