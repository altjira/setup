using FileAttribute;

// USAGE: carbon-setup-copy-helper from to mode uid gid
int main(string[] args) {
    string from = args[1];
    string to = args[2];
    int mode = int.parse(args[3]);
    int uid = int.parse(args[4]);
    int gid = int.parse(args[5]);

    File source_file = File.new_for_path(from);
    File dest_file = File.new_for_path(to);

    try { // Create the target's parent directory
        dest_file.get_parent().make_directory_with_parents();
    } catch (IOError.EXISTS ignored) {
        // It's okay
    } catch (Error e) {
        warning("Failed to create dir for target: %s: %s", to, e.message);
    }

    try { // Copy the file
        source_file.copy(dest_file, FileCopyFlags.ALL_METADATA & FileCopyFlags.OVERWRITE);
    } catch (Error e) {
        warning("Failed to copy file: %s -> %s: %s", from, to, e.message);
        return 1;
    }

    try { // Set permissions
        dest_file.set_attribute_uint32(UNIX_MODE, mode, FileQueryInfoFlags.NONE);
        dest_file.set_attribute_uint32(UNIX_UID, uid, FileQueryInfoFlags.NONE);
        dest_file.set_attribute_uint32(UNIX_GID, gid, FileQueryInfoFlags.NONE);
    } catch (Error e) {
        warning("Failed to set permissions: %s: %s", to, e.message);
        return 1;
    }

    try { // Delete the source
        source_file.@delete();
    } catch (Error e) {
        warning("Failed to delete: %s: %s", from, e.message);
        return 1;
    }

    return 0;
}
