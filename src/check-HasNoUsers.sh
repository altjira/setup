#!/usr/bin/env bash
# A little helper script used by polkit to decide if initial-setup tasks should be allowed

HasNoUsers=`busctl get-property org.freedesktop.Accounts /org/freedesktop/Accounts org.freedesktop.Accounts HasNoUsers`
if [[ "$HasNoUsers" == "b true" ]]; then
  exit 0
else
  exit 1
fi 
