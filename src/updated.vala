namespace Setup {

    // Proxies

    [DBus(name="sh.carbon.update1")]
    interface OSUpdated : DBusProxy {
        public abstract ObjectPath get_progress() throws Error;
        public abstract async void init_os(string target, string source) throws Error;
    }

    [DBus(name="sh.carbon.update1.Progress")]
    interface OSUpdatedProgress : DBusProxy {
        public signal void creating_dirs();
        public signal void pulling();
        public signal void downloaded_coarse(uint done, uint total);
        public signal void merging();
        public signal void deploying();
        public signal void pruning();
        public signal void finished();
    }

    // Getters that either return real proxies or the mock impl as appropriate

    async OSUpdated get_updated_proxy() {
        if (!Driver.mock) {
            try {
                var proxy = yield Bus.get_proxy<OSUpdated>(BusType.SYSTEM,
                    "sh.carbon.update1", "/sh/carbon/update1");
                if (proxy.g_name_owner == null)
                    error("Couldn't connect to os-updated");
                proxy.set_default_timeout(int.MAX); // Never time out
                return proxy;
            } catch (Error e) {
                error("Couldn't connect to os-updated: %s", e.message);
            }
        } else return new MockMainImpl();
    }

    async OSUpdatedProgress get_updated_progress(OSUpdated proxy) {
        if (!Driver.mock) {
            try {
                return yield Bus.get_proxy<OSUpdatedProgress>(BusType.SYSTEM,
                    "sh.carbon.update1", proxy.get_progress());
            } catch (Error e) {
                error("Couldn't get os-updated progress: %s", e.message);
            }
        } else {
            var progress = new MockProgressImpl();
            ((MockMainImpl) proxy).progress = progress;
            return progress;
        }
    }

    // Fake impl of os-updated that just simply sends pretty signals to the GUI

    private class MockProgressImpl : OSUpdatedProgress, DBusProxy {}
    private class MockMainImpl : OSUpdated, DBusProxy {
        public MockProgressImpl progress;

        ObjectPath get_progress() throws Error {
            return new ObjectPath("NOOP");
        }

        async void init_os(string target, string source) throws Error {
            print("os-updated: init_os %s from %s\n", target, source);

            progress.creating_dirs();
            yield sleep(500);
            progress.finished();

            progress.pulling();
            yield sleep(50);
            int count = 0;
            Timeout.add(50, () => {
                progress.downloaded_coarse(count, 100);
                if (count == 100) {
                    init_os.callback();
                    return Source.REMOVE;
                }
                count++;
                return Source.CONTINUE;
            });
            yield;
            progress.finished();

            progress.merging();
            yield sleep(600);
            progress.finished();

            progress.deploying();
            yield sleep(1500);
            progress.finished();

            progress.pruning();
            yield sleep(300);
            progress.finished();
        }

        private async void sleep(uint time) {
            Timeout.add(time, () => {
                sleep.callback();
                return Source.REMOVE;
            });
            yield;
        }
    }
}
