using Gnome.Languages;

namespace Setup.Lang {
    private static Gnome.XkbInfo xkb_info;

    public void init() {
        xkb_info = new Gnome.XkbInfo();
    }

    // For generating search keys from localized text
    [CCode(cname="cc_util_normalize_casefold_and_unaccent")]
    public extern string normalize_casefold_and_unaccent(string input);

    // Used when translating "Welcome" to different languages
    [CCode(cname="LC_MESSAGES_MASK", cheader_filename="locale.h")]
    public extern const int LC_MESSAGES_MASK;
    [CCode(cname="newlocale", cheader_filename="locale.h")]
    public extern void* newlocale(int mask, string id, void* base_locale);
    [CCode(cname="uselocale", cheader_filename="locale.h")]
    public extern void* uselocale(void* newlocale);
    [CCode(cname="freelocale", cheader_filename="locale.h")]
    public extern void freelocale(void* locale);

    // Tell localed about the selected locales & keyboard layouts
    [DBus(name="org.freedesktop.locale1")]
    private interface Localed : DBusProxy {
        public async abstract void set_locale(string[] locale, bool i)
            throws Error;
        public async abstract void set_x11_keyboard(string layout, string model,
            string variant, string options, bool convert, bool interactive)
            throws Error;
    }
    public async void set_localed_locale(string locale) {
        if (Driver.mock) {
            print("set_localed_locale: %s\n", locale);
            return;
        }

        try {
            var proxy = yield Bus.get_proxy<Localed>(BusType.SYSTEM,
                "org.freedesktop.locale1", "/org/freedesktop/locale1");
            yield proxy.set_locale({ locale }, true);
        } catch (Error e) {
            critical("Failed to set logind locale: %s", e.message);
        }
    }
    public async void set_localed_kbd(string layout, string variant) {
        if (Driver.mock) {
            print("set_localed_kbd: layout=%s, variant=%s\n", layout, variant);
            return;
        }

        try {
            var proxy = yield Bus.get_proxy<Localed>(BusType.SYSTEM,
                "org.freedesktop.locale1", "/org/freedesktop/locale1");
            yield proxy.set_x11_keyboard(layout, "", variant, "", true, true);
        } catch (Error e) {
            critical("Failed to set logind locale: %s", e.message);
        }
    }

    // Languages that show up in welcome spinner and as default list
    public const string[] INITIAL_LOCALES = {
        "en_US.UTF-8",
        //"de_DE.UTF-8",
        //"fr_FR.UTF-8",
        "es_ES.UTF-8",
        //"zh_CN.UTF-8",
        //"ja_JP.UTF-8",
        "ru_RU.UTF-8",
        //"ar_EG.UTF-8"
    };

    // One locale in the LocaleModel
    private class Locale : Object {
        public string locale_id;

        // Display names
        public string lang_name;
        public string? country_name = null;

        // Search names
        public string? locale_name;
        public string? locale_current_name;
        public string? locale_untranslated_name;

        // Filter & sort
        public bool is_extra;
        public string? sort_key = null;

        public Locale(string id) {
            locale_id = id;

            string lang; // Name of the language (i.e. en)
            string? country; // Name of the region (i.e. US)
            parse_locale(id, out lang, out country, null, null);

            lang_name = get_language_from_code(lang, id) ??
                        get_language_from_code(lang, null);
            if (country != null) {
                country_name = get_country_from_code(country, id) ??
                               get_country_from_code(country, null);
            }

            is_extra = !(id in INITIAL_LOCALES);
            locale_name = get_language_from_locale(id, id);
            locale_current_name = get_language_from_locale(id, null);
            locale_untranslated_name = get_language_from_locale(id, "C");
            if (locale_name != null)
                sort_key = normalize_casefold_and_unaccent(locale_name);
        }

        // Special handling for more button and empty label
        public bool is_more = false;
        public Locale.more_btn() {
           is_more = true;
        }
    }

    // One keyboard layout in KbdModel
    private class KbdLayout : Object {
        public string kind;
        public string id;
        public string? display_name = null;

        // xkb settings
        public string? xkb_layout = null;
        public string? xkb_variant = null;

        // ibus settings
        // TODO

        // Filter & sort
        public bool is_primary = false;
        public bool is_locale = false;

        public KbdLayout(string kind, string id) {
            this.kind = kind;
            this.id = id;

            if (this.kind == "xkb") {
                xkb_info.get_layout_info(id, out display_name, null,
                    out xkb_layout, out xkb_variant);
            } else {
                error("Can't handle ibus input methods yet!"); // TODO
            }
        }

        public bool is_more = false;
        public KbdLayout.more_btn() {
           is_more = true;
        }
    }

    // A ListModel of all the locales on the system (with a more item)
    private class LocaleModel : ListModel, Object {
        private ListStore base_model;
        private Gtk.Filter filter;
        private Gtk.FilterListModel filter_model;

        private bool more_visible = false;
        private string filter_text = "";

        construct {
            // Create, populate, and sort the base model
            base_model = new ListStore(typeof(Locale));
            foreach (var l in get_all_locales())
                base_model.append(new Locale(l));
            base_model.sort((a, b) => sort_func(a as Locale, b as Locale));

            // Setup the filter
            filter = new Gtk.CustomFilter(it => filter_func(it as Locale));
            filter_model = new Gtk.FilterListModel(base_model, filter);

            // Delegate the items_changed signal to the filter_model
            filter_model.items_changed.connect((pos, rm, add) => {
                this.items_changed(pos, rm, add);
            });
        }

        private bool filter_func(Locale item) {
            // Hide extra rows if more button wasn't clicked
            if (item.is_extra && !more_visible) return false;

            // Filter based on search box text
            if (item.locale_name != null &&
                filter_text.match_string(item.locale_name, true))
                return true;
            if (item.locale_current_name != null &&
                filter_text.match_string(item.locale_current_name, true))
                return true;
            if (item.locale_untranslated_name != null &&
                filter_text.match_string(item.locale_untranslated_name, true))
                return true;
            return false;
        }

        private int sort_func(Locale a, Locale b) {
            // Extra languages always go after initial languages
            if (a.is_extra && !b.is_extra) return 1;
            if (!a.is_extra && b.is_extra) return -1;

            // Sort by sort key if available
            if (a.sort_key != null && b.sort_key != null) {
                var sort_key = strcmp(a.sort_key, b.sort_key);
                if (sort_key != 0) return sort_key;
            }

            // Fallback to sort by locale ID
            return strcmp(a.locale_id, b.locale_id);
        }

        Type get_item_type() {
            return typeof(Locale);
        }

        uint get_n_items() {
            return filter_model.get_n_items() + (more_visible ? 0 : 1);
        }

        Object? get_item(uint pos) {
            if (pos < filter_model.get_n_items())
                return filter_model.get_item(pos);
            return new Locale.more_btn();
        }

        // Shows the whole language list and hides the more button
        public void show_more() {
            items_changed(get_n_items() - 1, 1, 0); // Delete the more item
            this.more_visible = true;
            filter.changed(Gtk.FilterChange.LESS_STRICT);
        }

        // Filters the language list
        public void update_filter(string text) {
            this.filter_text = text;
            filter.changed(Gtk.FilterChange.DIFFERENT);
        }
    }

    // A ListModel of all the keyboard layouts on the system (with a more item)
    private class KbdModel : ListModel, Object {
        private ListStore base_model;
        private Gtk.Filter filter;
        private Gtk.FilterListModel filter_model;
        private GenericSet<string> seen_inputs;
        private string filter_text = "";

        construct {
            // Create the base model
            base_model = new ListStore(typeof(KbdLayout));

            // Setup the filter
            filter = new Gtk.CustomFilter(it => filter_func(it as KbdLayout));
            filter_model = new Gtk.FilterListModel(base_model, filter);

            // Delegate the items_changed signal to the filter_model
            filter_model.items_changed.connect((pos, rm, add) => {
                this.items_changed(pos, rm, add);
            });
        }

        private bool filter_func(KbdLayout item) {
            // Filter based on search box text
            if (item.display_name != null &&
                filter_text.match_string(item.display_name, true))
                return true;
            return false;
        }

        private int sort_func(KbdLayout a, KbdLayout b) {
            // The primary layout always goes first
            if (a.is_primary && !b.is_primary) return -1;
            if (!a.is_primary && b.is_primary) return 1;

            // Locale layouts always go next
            if (a.is_locale && !b.is_locale) return -1;
            if (!a.is_locale && b.is_locale) return 1;

            // The rest of the layouts are sorted alphabetically
            if (a.display_name != null && b.display_name != null) {
                var cmp = strcmp(a.display_name, b.display_name);
                if (cmp != 0) return cmp;
            }

            // Fallback to sort by id
            return strcmp(a.id, b.id);
        }

        Type get_item_type() {
            return typeof(KbdLayout);
        }

        uint get_n_items() {
            return filter_model.get_n_items();
        }

        Object? get_item(uint pos) {
            if (pos < filter_model.get_n_items())
                return filter_model.get_item(pos);
            assert_not_reached();
        }

        private void add_rows_to_list(List<weak string> list, string kind,
                bool is_locale) {
            foreach (var item in list) {
                var key = "%s::%s".printf(kind, item);

                if (key in seen_inputs) continue;
                seen_inputs.add(key);

                var layout = new KbdLayout(kind, item);
                layout.is_primary = false;
                layout.is_locale = is_locale;
                base_model.insert_sorted(layout, (a, b) => {
                    return sort_func(a as KbdLayout, b as KbdLayout);
                });
            }
        }

        public void repopulate(string? locale) {
            base_model.remove_all();
            seen_inputs = new GenericSet<string>(str_hash, str_equal);
            if (locale == null) return;

            // Layouts to ignore
            seen_inputs.add("xkb::custom");

            string lang; // Name of the language (i.e. en)
            string? country; // Name of the region (i.e. US)
            parse_locale(locale, out lang, out country, null, null);

            // Add default input source for locale
            string? def_kind = null, def_id = null;
            get_input_source_from_locale(locale, out def_kind, out def_id);
            if (def_kind != "xkb") def_kind = null; // TODO
            if (def_kind != null && def_id != null) {
                seen_inputs.add("%s::%s".printf(def_kind, def_id));
                var layout = new KbdLayout(def_kind, def_id);
                layout.is_primary = true;
                layout.is_locale = false;
                base_model.append(layout);
            }

            // Add layouts for language
            var layouts = xkb_info.get_layouts_for_language(lang);
            add_rows_to_list(layouts, "xkb", true);

            // Add layouts for country
            if (country != null) {
                layouts = xkb_info.get_layouts_for_country(country);
                add_rows_to_list(layouts, "xkb", true);
            }

            // Add the rest
            layouts = xkb_info.get_all_layouts();
            add_rows_to_list(layouts, "xkb", false);
        }

        public void update_filter(string text) {
            this.filter_text = text;
            filter.changed(Gtk.FilterChange.DIFFERENT);
        }
    }

    // Language picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/language-picker.ui")]
    class Picker : Gtk.Box {
        private LocaleModel model;
        [GtkChild] unowned Gtk.Entry search_box;
        [GtkChild] unowned Gtk.ListBox list_box;
        [GtkChild] unowned Gtk.Label empty_state;
        [GtkChild] unowned Gtk.Image more_row;
        [GtkChild] unowned Gtk.Stack placeholder;
        [GtkChild] unowned Gtk.Revealer search_revealer;

        public string selected_locale { get; set; }

        construct {
            model = new LocaleModel();
            model.items_changed.connect(on_model_changed);
            on_model_changed();
            list_box.bind_model(model, it => create_row(it as Locale));
        }

        public void populate_text() {
            // TRANSLATORS: Found here means that the user searched for a
            // language that isn't in the list.
            empty_state.label = _("No Languages Found");
            search_box.placeholder_text = _("Search");
            more_row.tooltip_text = _("More…");
        }

        private Gtk.Widget create_row(Locale l) {
            if (l.is_more) return more_row;
            return new LocaleRow(this, l);
        }

        private void on_model_changed() {
            if (model.get_n_items() == 0)
                placeholder.visible_child = empty_state;
            else
                placeholder.visible_child = list_box;
        }

        [GtkCallback]
        private void on_search_changed() {
            model.update_filter(search_box.text);
        }

        [GtkCallback]
        private void on_activated(Gtk.ListBoxRow row) {
            if (row.child == more_row) { // Handle the more item
                search_revealer.reveal_child = true;
                search_box.grab_focus();
                model.show_more();
                return;
            }

            var id = ((LocaleRow) row).locale.locale_id;
            if (selected_locale != id) {
                selected_locale = id;
            } else confirm();
        }

        public signal void confirm();
    }

    // Keyboard picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/kbd-picker.ui")]
    class KbdPicker : Gtk.Box {
        private KbdModel model;
        [GtkChild] unowned Gtk.Entry search_box;
        [GtkChild] unowned Gtk.ListBox list_box;
        [GtkChild] unowned Gtk.Label empty_state;
        [GtkChild] unowned Gtk.Stack placeholder;

        public string? selected_kind { get; set; default = null; }
        public string? selected_id { get; set; default = null; }

        construct {
            model = new KbdModel();
            model.items_changed.connect(on_model_changed);
            on_model_changed();
            list_box.bind_model(model, it => new KbdRow(this, it as KbdLayout));
        }

        public void update_locale(string? locale) {
            // TRANSLATORS: Found here means that the user searched for a
            // keyboard layout that isn't in the list.
            empty_state.label = _("No Keyboard Layouts Found");
            search_box.placeholder_text = _("Search");

            // De-select
            selected_kind = null;
            selected_id = null;

            // Change sort order & etc
            Idle.add(() => {
                model.repopulate(locale);
                return Source.REMOVE;
            });
            if (locale != null) {
                string kind, id;
                get_input_source_from_locale(locale, out kind, out id);
                selected_kind = kind;
                selected_id = id;
            }
        }

        private void on_model_changed() {
            if (model.get_n_items() == 0)
                placeholder.visible_child = empty_state;
            else
                placeholder.visible_child = list_box;
        }

        [GtkCallback]
        private void on_search_changed() {
            model.update_filter(search_box.text);
        }

        [GtkCallback]
        private void on_activated(Gtk.ListBoxRow row) {
            var layout = ((KbdRow) row).layout;
            if (selected_kind != layout.kind || selected_id != layout.id) {
                selected_kind = layout.kind;
                selected_id = layout.id;
            } else confirm();
        }

        public signal void confirm();
    }

    // One language in the language picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/locale-row.ui")]
    class LocaleRow : Gtk.ListBoxRow {
        private Picker picker;
        public Locale locale;

        [GtkChild] unowned Gtk.Label lang_label;
        [GtkChild] unowned Gtk.Label country_label;
        [GtkChild] unowned Gtk.Image checkmark;

        public LocaleRow(Picker picker, Locale locale) {
            this.picker = picker;
            this.locale = locale;
            lang_label.label = locale.lang_name;
            country_label.label = locale.country_name;
            picker.notify["selected-locale"].connect(update_check);
            update_check();
        }

        private void update_check() {
            var selected = (picker.selected_locale == locale.locale_id);
            checkmark.opacity = selected ? 1 : 0;
        }
    }

    // One keyboard input in the keyboard picker widget
    [GtkTemplate(ui="/sh/carbon/setup/ui/kbd-row.ui")]
    class KbdRow : Gtk.ListBoxRow {
        private KbdPicker picker;
        public KbdLayout layout;

        [GtkChild] unowned Gtk.Label label;
        [GtkChild] unowned Gtk.Image checkmark;
        [GtkChild] unowned Gtk.Button preview_btn;

        public KbdRow(KbdPicker picker, KbdLayout layout) {
            this.picker = picker;
            this.layout = layout;
            label.label = layout.display_name;
            preview_btn.visible = (layout.kind == "xkb");
            picker.notify["selected-kind"].connect(update_check);
            picker.notify["selected-id"].connect(update_check);
            update_check();
        }

        private void update_check() {
            var selected = (picker.selected_kind == layout.kind &&
                            picker.selected_id == layout.id);
            checkmark.opacity = selected ? 1 : 0;
        }

        [GtkCallback]
        void open_preview() {
            string cmdline;
            if (layout.xkb_variant != null)
                cmdline = "gkbd-keyboard-display -l \"%s\t%s\""
                    .printf(layout.xkb_layout, layout.xkb_variant);
            else
                cmdline = "gkbd-keyboard-display -l \"%s\""
                    .printf(layout.xkb_layout);
            try {
                Process.spawn_command_line_async(cmdline);
            } catch (Error e) {
                warning("Failed to spawn process: %s", e.message);
            }
        }
    }
}
