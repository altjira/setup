interface Setup.Page : Adw.Bin {
    // Whether or not the continue button should be sensitive
    public abstract bool ready { get; }

    // Whether or not the page can be skipped
    public virtual bool skippable { get { return false; } }

    // Whether or not the back button should be hidden
    public virtual bool hide_back { get { return false; } }

    // Whether or not the continue button should be hidden
    public virtual bool hide_next { get { return false; } }

    // Whether or not the page wants a reboot button instead of next/done
    // This is only visual. The page should do the reboot in apply()
    public virtual bool wants_reboot { get { return false; } }

    // The user entered this page
    public virtual void enter() {
        // A noop for most things
    }

    // The user pressed next. Apply the settings on the page
    // Return true to continue, false to wait
    public virtual bool apply() {
        // A noop for most things
        return true;
    }

    public abstract void populate_text();

    // Allows the page to ask the shell to continue
    public signal void next();

    // Allows the page to ask the shell to quit
    public signal void quit();
}
