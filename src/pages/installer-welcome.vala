[GtkTemplate(ui="/sh/carbon/setup/ui/installer-welcome.ui")]
class Setup.InstallerWelcomePage : Adw.Bin, Setup.Page {
    public bool ready { get { return true; } }

    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Label subtitle;
    [GtkChild] unowned Gtk.Image logo;
    [GtkChild] unowned Gtk.Button next_btn;
    [GtkChild] unowned Gtk.Button cancel_btn;

    void populate_text() {
        // TRANSLATORS: This describes what the window is/does. Not a command
        title.label = _("Install carbonOS");
        subtitle.label = _("We're glad you like it!");
        next_btn.label = _("_Begin");
        cancel_btn.label = _("_Cancel");
    }

    void enter() {
        var style_mgr = Adw.StyleManager.get_default();
        style_mgr.notify["dark"].connect(() => update_style(style_mgr));
        update_style(style_mgr);
    }

    private void update_style(Adw.StyleManager mgr) {
        if (mgr.dark)
            logo.icon_name = "carbon-logo-dark";
        else
            logo.icon_name = "carbon-logo";
    }

    [GtkCallback]
    void on_next() {
        this.next();
    }

    [GtkCallback]
    void on_quit() {
        this.quit();
    }
}
