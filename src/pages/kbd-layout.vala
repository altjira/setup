using Setup.Lang;

[GtkTemplate(ui="/sh/carbon/setup/ui/kbd-layout.ui")]
class Setup.KbdLayoutPage : Adw.Bin, Setup.Page {
    private bool _ready = false;
    public bool ready { get { return _ready; } }
    public bool skippable { get { return false; } }

    private Driver driver;
    private Settings wf_settings;
    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Gtk.Entry test_entry;
    [GtkChild] unowned KbdPicker picker;

    public KbdLayoutPage(Driver driver) {
        this.driver = driver;
        wf_settings = new Settings("org.wayfire.section.input");
    }

    void populate_text() {
        title.label = _("Keyboard");
        test_entry.placeholder_text = _("Test your settings here");
        picker.update_locale(driver.language);
    }

    [GtkCallback]
    void on_layout_selected() {
        _ready = true;
        this.notify_property("ready");

        // Update wayfire's settings to preview the layout
        if (picker.selected_kind == "xkb") {
            string? layout, variant;
            xkb_info.get_layout_info(picker.selected_id, null, null,
                out layout, out variant);

            wf_settings.set_string("xkb-layout", layout);
            wf_settings.set_string("xkb-variant", variant ?? "");
        }
    }

    [GtkCallback]
    void on_next() {
        this.next();
    }

    bool apply() {
        var kind = picker.selected_kind;
        var id = picker.selected_id;

        if (kind == "xkb") {
            string? layout, variant;
            xkb_info.get_layout_info(id, null, null, out layout, out variant);

            // Tell logind about the keyboard layout
            set_localed_kbd.begin(layout, variant);
        }


        return true;
    }
}
