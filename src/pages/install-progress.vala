[GtkTemplate(ui="/sh/carbon/setup/ui/install-progress.ui")]
class Setup.InstallProgressPage : Adw.Bin, Setup.Page {
    public bool ready { get { return false; } }
    public bool hide_next { get { return true; } }
    public bool hide_back { get { return true; } }

    private Driver driver;
    private uint pulsing = 0;
    [GtkChild] unowned Gtk.ProgressBar pbar;
    [GtkChild] unowned Gtk.Label status;
    [GtkChild] unowned Gtk.MessageDialog error_dialog;

    // These are the block devices / partitions / filesystems we are working on
    private UDisks.Object esp;
    private new UDisks.Object root;

    public InstallProgressPage(Driver driver) {
        this.driver = driver;
        error_dialog.transient_for = driver.shell;
    }

    void populate_text() {
        // Nothing to do here
    }

    private async OSUpdated setup_updated() {
        var proxy = yield get_updated_proxy();
        var progress = yield get_updated_progress(proxy);
        progress.creating_dirs.connect(() => {
            // TRANSLATORS: Install step: creating filesystem layout
            status.label = _("Initializing…");
            set_pulse(true);
        });
        progress.pulling.connect(() => {
            // TRANSLATORS: Install step: Copying the OS
            status.label = _("Copying Files…");
            set_pulse(true);
        });
        progress.downloaded_coarse.connect((done, total) => {
            var frac = (double) done / total;
            var percent = (int)(frac * 100);
            if (percent > 0) {
                set_pulse(false);
                // TRANSLATORS: Install step: copying w/ percent complete
                status.label = _("Copying Files… (%d%%)").printf(percent);
                pbar.fraction = frac;
            }
        });
        progress.merging.connect(() => {
            // TRANSLATORS: Install step: merging OS and kernel into final image
            status.label = _("Merging…");
            set_pulse(true);
        });
        progress.deploying.connect(() => {
            // TRANSLATORS: Install step: Turning the image into a bootable OS
            status.label = _("Deploying…");
            set_pulse(true);
        });
        progress.pruning.connect(() => {
            // TRANSLATORS: Install step: deleting temporary files
            status.label = _("Cleaning up…");
            set_pulse(true);
        });
        progress.finished.connect(() => {
            set_pulse(false);
        });

        // Don't lose the reference
        proxy.set_data("progress", progress);
        return proxy;
    }

    // Returns the partition type UUID for a rootfs on the current arch
    // Reference: https://www.freedesktop.org/software/systemd/man/systemd-gpt-auto-generator.html#id-1.5.7
    private string get_root_part_type() {
        var arch = Posix.utsname().machine;
        switch (arch) {
            case "x86_64":
                return "4f68bce3-e8cd-4db1-96e7-fbcaf984b709";
            case "aarch64":
                return "b921b045-1df0-41c3-af44-4c6f280d3fae";
            default:
                error("Unknown arch: " + arch);
        }
    }

    private async void format_drive() throws Error {
        // TRANSLATORS: Install step: Wiping disk & creating partitions
        status.label = _("Formatting…");
        if (Driver.mock) {
            yield mock_progress(1500);
            return;
        }
        set_pulse(true);

        // Format the drive
        var opts = new HashTable<string, Variant>(str_hash, str_equal);
        UDisks.Block block = driver.target_drive.get_data("block");
        yield block.call_format("gpt", opts, null);
        yield settle();
        var ptable_obj_path = ((DBusProxy) block).g_object_path;
        var ptable = driver.udisks.get_object(ptable_obj_path).partition_table;

        // Create the EFI System Partition (unformatted)
        // mount-helper will format this for us. See comment there for reasoning
        string esp_obj_path;
        yield ptable.call_create_partition(
            0, 524288000 /* 500 MiB */, "c12a7328-f81f-11d2-ba4b-00a0c93ec93b",
            "esp", opts, null, out esp_obj_path
        );
        yield settle();
        esp = driver.udisks.get_object(esp_obj_path);

        // Create the root filesystem (btrfs)
        string root_obj_path;
        opts["label"] = "carbonOS";
        yield ptable.call_create_partition_and_format(
            esp.partition.offset + esp.partition.size, 0 /* fill */,
            get_root_part_type(), "carbon-root", opts, "btrfs", opts, null,
            out root_obj_path
        );
        yield settle();
        root = driver.udisks.get_object(root_obj_path);

        // Format the ESP and mount everything into /mnt
        var mount_helper = new Subprocess(SubprocessFlags.NONE, "pkexec",
            LIBEXEC + "/carbon-setup-mount-helper", root.block.device,
            esp.block.device);
        yield mount_helper.wait_check_async();

        // Ensure the ESP object has a reference to the fs the mount-helper made
        esp = driver.udisks.get_object(esp_obj_path);

        set_pulse(false);
    }

    private async void install_bootloader() throws Error {
        // TRANSLATORS: Install step: Installing the bootloader
        status.label = _("Installing bootloader…");
        if (Driver.mock) {
            yield mock_progress(600);
            return;
        }
        set_pulse(true);

        string[] cmd = {
            "pkexec",
            BOOTCTL, "install",
            "--esp-path=/mnt/boot",
            "--make-machine-id-directory=no",
            null };
        var proc = new Subprocess.newv(cmd, SubprocessFlags.NONE);
        yield proc.wait_check_async();

        set_pulse(false);
    }

    private async void umount() throws Error {
        // TRANSLATORS: Install step: Shutting down disk we just installed to
        status.label = _("Finalizing…");
        if (Driver.mock) {
            yield mock_progress(500);
            return;
        }
        set_pulse(true);

        var opts = new HashTable<string, Variant>(str_hash, str_equal);
        yield esp.filesystem.call_unmount(opts, null);
        yield root.filesystem.call_unmount(opts, null);

        set_pulse(false);
    }

    private async void install_the_os() throws Error {
        // Spin up os-updated
        var proxy = yield setup_updated();

        // Format the drive & mount the new partitions
        yield format_drive();

        // Create the OSTree layout, populate it, and deploy
        yield proxy.init_os("/mnt", "");

        // Install systemd-boot
        yield install_bootloader();

        // Unmount & flush everything
        yield umount();

        // Show the confirmation page
        next();
    }

    private void set_pulse(bool active) {
        if (active) {
            if (pulsing != 0) return;
            pulsing = Timeout.add(10, () => {
                pbar.pulse();
                return Source.CONTINUE;
            });
        } else {
            if (pulsing == 0) return;
            Source.remove(pulsing);
            pulsing = 0;
        }
    }

    // Waits for all of the DBus calls to syncronize before continuing
    private async void settle() {
        Idle.add(() => {
            settle.callback();
            return Source.REMOVE;
        });
        yield;
    }

    // Simply spins the progress bar for a set amount of time to simulate activity
    private async void mock_progress(uint time) {
        set_pulse(true);
        Timeout.add(time, () => {
            mock_progress.callback();
            return Source.REMOVE;
        });
        yield;
        set_pulse(false);
    }

    [GtkCallback]
    void on_error_dialog_dismissed(int response) {
        driver.reboot.begin();
        driver.shell.destroy();
    }

    void enter() {
        install_the_os.begin((obj, res) => {
            try {
                install_the_os.end(res);
            } catch (Error e) {
                critical("Failed to install! %s", e.message);
                set_pulse(false);

                error_dialog.visible = true;
                // TODO: Show an error dialog to force shutdown
            }
        });
    }
}
