[GtkTemplate(ui="/sh/carbon/setup/ui/try-or-install.ui")]
class Setup.TryOrInstallPage : Adw.Bin, Setup.Page {
    public bool ready { get { return false; } }
    public bool skippable { get { return false; } }
    public bool hide_next { get { return true; } }

    private Driver driver;
    [GtkChild] unowned Gtk.Label title;
    [GtkChild] unowned Adw.ActionRow try_row;
    [GtkChild] unowned Adw.ActionRow install_row;
    [GtkChild] unowned Adw.ActionRow repair_row;


    public TryOrInstallPage(Driver driver) {
        this.driver = driver;
    }

    void populate_text() {
        title.label = _("Try or Install?");

        // TRANSLATORS: "Try" here means "Try carbonOS and see if you like it"
        // It's a demo or trial of the system
        try_row.title = _("_Try");
        try_row.subtitle = _("Changes will not be saved. Your existing data will not be modified. Performance and features may be limited");

        install_row.title = _("_Install");
        install_row.subtitle = _("Wipe the disk and install a fresh copy of carbonOS");

        repair_row.title = _("_Repair");
        repair_row.subtitle = _("Repair an existing installation of carbonOS");
    }

    [GtkCallback]
    void on_try() {
        driver.start_live_session.begin();
    }

    [GtkCallback]
    void on_install() {
        next();
    }

    [GtkCallback]
    void on_repair() {
        // TODO: Unimplemented
        var dialog = new Gtk.MessageDialog(driver.shell, Gtk.DialogFlags.MODAL,
            Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, null);
        dialog.text = _("Feature isn't implemented");
        dialog.secondary_text = _("Setup cannot repair carbonOS installations yet");
        dialog.show();
        dialog.response.connect(dialog.destroy);
    }
}
