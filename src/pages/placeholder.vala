class Setup.PlaceholderPage : Adw.Bin, Setup.Page {
    public bool ready { get { return true; } }
    public bool skippable { get { return true; } }

    public PlaceholderPage(string msg) {
        var label = new Gtk.Label(msg);
        label.add_css_class("title-1");
        this.set_child(label);
    }

    void populate_text() {
        // Noop
    }
}
