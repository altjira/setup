class Setup.Greeter : Object {
    private LightDM.Greeter? greeter;
    private string? password = null;

    construct {
        // Connect to the greeter
        if (!Driver.mock) {
            greeter = new LightDM.Greeter();
            try {
                greeter.connect_to_daemon_sync();
                greeter.authentication_complete.connect(on_auth_complete);
                greeter.show_message.connect(on_show_message);
                greeter.show_prompt.connect(on_show_prompt);
            } catch (Error e) {
                critical("Failed to connect to LightDM: %s", e.message);
                greeter = null;
            }
        } else {
            print("Greeter: connect\n");
            greeter = null;
        }
    }

    public async void auth(string username, string? pass) {
        var cb = this.done.connect(() => auth.callback());
        auth_sync(username, pass);
        yield;
        this.disconnect(cb);
    }

    public void start_session(bool live) {
        if (greeter == null) { // No-op impl
            print("Greeter: start_session\n");
            return;
        }

        try {
            greeter.start_session_sync(live ? "graphite-live" : "graphite");
        } catch (Error e) {
            error("Failed to start session: %s", e.message);
        }
    }

    private signal void done();

    private void auth_sync(string username, string? pass) {
        if (greeter == null) { // No-op impl
            print("Greeter: auth '%s', password '%s'\n", username, pass);
            Idle.add(() => {
                done();
                return Source.REMOVE;
            });
            return;
        }

        this.password = pass ?? "";
        try {
            greeter.authenticate(username);
        } catch (Error e) {
            critical("Couldn't start authentication: %s", e.message);
            done();
        }
    }

    private void on_show_message(string text, LightDM.MessageType type) {
        if (type == LightDM.MessageType.ERROR)
            warning("LightDM ERROR: %s", text);
        else
            message("LightDM INFO: %s", text);
    }

    private void on_show_prompt(string text, LightDM.PromptType type) {
        try {
            greeter.respond(password);
        } catch (Error e) {
            error("Failed to respond to greeter prompt: %s", e.message);
        }
    }

    private void on_auth_complete() {
        if (greeter.is_authenticated)
            done();
        else
            error("Failed to authenticate");
    }
}
