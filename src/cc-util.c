/*
 * Copyright (c) 2012 Giovanni Campagna <scampa.giovanni@gmail.com>
 *
 * The Control Center is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * The Control Center is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with the Control Center; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <glib.h>
#include <glib/gi18n.h>
#include <string.h>

/* Combining diacritical mark?
 *  Basic range: [0x0300,0x036F]
 *  Supplement:  [0x1DC0,0x1DFF]
 *  For Symbols: [0x20D0,0x20FF]
 *  Half marks:  [0xFE20,0xFE2F]
 */
#define IS_CDM_UCS4(c) (((c) >= 0x0300 && (c) <= 0x036F)  || \
                        ((c) >= 0x1DC0 && (c) <= 0x1DFF)  || \
                        ((c) >= 0x20D0 && (c) <= 0x20FF)  || \
                        ((c) >= 0xFE20 && (c) <= 0xFE2F))

/*
 * Copied from gnome-control-center/panels/common/cc-util.c under the GPL
 * And then from tracker/src/libtracker-fts/tracker-parser-glib.c
 * And then from gnome-shell/src/shell-util.c
 *
 * Originally written by Aleksander Morgado <aleksander@gnu.org>
 */
char *
cc_util_normalize_casefold_and_unaccent (const char *str)
{
  char *normalized, *tmp;
  int i = 0, j = 0, ilen;

  if (str == NULL)
    return NULL;

  normalized = g_utf8_normalize (str, -1, G_NORMALIZE_NFKD);
  tmp = g_utf8_casefold (normalized, -1);
  g_free (normalized);

  ilen = strlen (tmp);

  while (i < ilen)
    {
      gunichar unichar;
      gchar *next_utf8;
      gint utf8_len;

      /* Get next character of the word as UCS4 */
      unichar = g_utf8_get_char_validated (&tmp[i], -1);

      /* Invalid UTF-8 character or end of original string. */
      if (unichar == (gunichar) -1 ||
          unichar == (gunichar) -2)
        {
          break;
        }

      /* Find next UTF-8 character */
      next_utf8 = g_utf8_next_char (&tmp[i]);
      utf8_len = next_utf8 - &tmp[i];

      if (IS_CDM_UCS4 ((guint32) unichar))
        {
          /* If the given unichar is a combining diacritical mark,
           * just update the original index, not the output one */
          i += utf8_len;
          continue;
        }

      /* If already found a previous combining
       * diacritical mark, indexes are different so
       * need to copy characters. As output and input
       * buffers may overlap, need to use memmove
       * instead of memcpy */
      if (i != j)
        {
          memmove (&tmp[j], &tmp[i], utf8_len);
        }

      /* Update both indexes */
      i += utf8_len;
      j += utf8_len;
    }

  /* Force proper string end */
  tmp[j] = '\0';

  return tmp;
}

/*============================================================================*/
/* Copied from gnome-control-center/panels/common/hostname-helper.c */
/* https://gitlab.gnome.org/GNOME/gnome-control-center/-/blob/c114828e/panels/common/hostname-helper.c */
/*============================================================================*/

static char *
allowed_chars (void)
{
	GString *s;
	char i;

	s = g_string_new (NULL);
	for (i = 'a'; i <= 'z'; i++)
		g_string_append_c (s, i);
	for (i = 'A'; i <= 'Z'; i++)
		g_string_append_c (s, i);
	for (i = '0'; i <= '9'; i++)
		g_string_append_c (s, i);
	g_string_append_c (s, '-');

	return g_string_free (s, FALSE);
}

static char *
remove_leading_dashes (char *input)
{
	char *start;

	for (start = input; *start && (*start == '-'); start++)
		;

	memmove (input, start, strlen (start) + 1);

	return input;
}

static gboolean
is_empty (const char *input)
{
	if (input == NULL ||
	    *input == '\0')
		return TRUE;
	return FALSE;
}

static char *
remove_trailing_dashes (char *input)
{
	int len;

	len = strlen (input);
	while (len--) {
		if (input[len] == '-')
			input[len] = '\0';
		else
			break;
	}
	return input;
}

static char *
remove_apostrophes (char *input)
{
	char *apo;

	while ((apo = strchr (input, '\'')) != NULL)
		memmove (apo, apo + 1, strlen (apo));
	return input;
}

static char *
remove_duplicate_dashes (char *input)
{
	char *dashes;

	while ((dashes = strstr (input, "--")) != NULL)
		memmove (dashes, dashes + 1, strlen (dashes));
	return input;
}

#define CHECK	if (is_empty (result)) return g_strdup ("localhost")

char *
cc_util_pretty_hostname_to_static (const char *pretty,
			   gboolean    for_display)
{
	g_autofree gchar *result = NULL;
	g_autofree gchar *valid_chars = NULL;
	g_autofree gchar *composed = NULL;

	g_return_val_if_fail (pretty != NULL, NULL);
	g_return_val_if_fail (g_utf8_validate (pretty, -1, NULL), NULL);

	g_debug ("Input: '%s'", pretty);

	composed = g_utf8_normalize (pretty, -1, G_NORMALIZE_ALL_COMPOSE);
	g_debug ("\tcomposed: '%s'", composed);
	/* Transform the pretty hostname to ASCII */
	result = g_str_to_ascii (composed, NULL);
	g_debug ("\ttranslit: '%s'", result);

	CHECK;

	/* Remove apostrophes */
	remove_apostrophes (result);
	g_debug ("\tapostrophes: '%s'", result);

	CHECK;

	/* Remove all the not-allowed chars */
	valid_chars = allowed_chars ();
	g_strcanon (result, valid_chars, '-');
	g_debug ("\tcanon: '%s'", result);

	CHECK;

	/* Remove the leading dashes */
	remove_leading_dashes (result);
	g_debug ("\tleading: '%s'", result);

	CHECK;

	/* Remove trailing dashes */
	remove_trailing_dashes (result);
	g_debug ("\ttrailing: '%s'", result);

	CHECK;

	/* Remove duplicate dashes */
	remove_duplicate_dashes (result);
	g_debug ("\tduplicate: '%s'", result);

	CHECK;

	/* Lower case */
	if (!for_display)
		return g_ascii_strdown (result, -1);

	return g_steal_pointer (&result);
}

