// Functions to facilitate the transfer of data from the greeter user to the new target user
namespace Setup {
    private struct DataFile { string name; int mode; }
    private const DataFile[] DATA_FILES = {
        { "/.config/dconf/user", 0644 }, // gsettings storage
        { "/.config/goa-1.0/accounts.conf", 0644 }, // online accounts
        { "/.local/share/keyrings/login.keyring", 0600 } // passwords
    };

    // Delete the file, ignoring any errors
    private void maybe_delete_file(string file) {
        if (Driver.mock) {
            print("copy: maybe_delete_file: %s\n", file);
            return;
        }
        FileUtils.unlink(file);
    }

    // Ensures that no user-specific data remains in the greeter's user
    void ensure_clean() {
        if (Environment.get_variable("SETUP_NO_MOCK") != null)
            return; // Do nothing if we're running in a user session
        foreach (var file in DATA_FILES)
            maybe_delete_file(Environment.get_home_dir() + file.name);
    }

    private async void copy_file(string from, string to, int mode, NewUser owner)
            throws Error {
        if (Driver.mock) {
            print("copy: copy_file: %s -> %s, mode=%d, owner=%s\n", from, to,
                mode, owner.user_name);
            return;
        }

        var copy_helper = new Subprocess(SubprocessFlags.NONE, "pkexec",
            LIBEXEC + "/carbon-setup-copy-helper", from, to, mode.to_string(),
            owner.uid.to_string(), owner.gid.to_string());
        yield copy_helper.wait_check_async();
    }

    // Moves the data files into the new user's home directory
    async void copy_content(NewUser user) {
        foreach (var file in DATA_FILES) {
            try {
                var from = Environment.get_home_dir() + file.name;
                var to = user.homedir + file.name;
                yield copy_file(from, to, file.mode, user);
            } catch (Error e) {
                critical("Failed to process file '%s': %s", file.name, e.message);
            }
        }
    }
}
